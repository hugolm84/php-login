<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title><?php echo Config::website_name; ?></title>
		<link href="css/main.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div id="pagewrap">
		<!--Header -->
		<header id="header">
			<hgroup>
				<h1 id="site-logo">
					<a href="index.html"><?php echo Config::website_name; ?></a>
				</h1>
			</hgroup>
			<!-- Navigation -->
			<nav>
				<ul class="clearfix">
					<li class="selected"><a href="?">Home</a></li>
					<?php if(user::loggedIn()): ?>
						<li class="selected"><a href="?p=account">My Account</a></li>
						<?php if(user::isAdmin()) : ?>
							<li><a href="?p=admin">Admin</a></li>
						<?php endif; ?>
					<li><a href="index.php?a=logout">Logout</a></li>
					<?php endif; ?>
					<li><a href="index?p=about">About</a></li>
				</ul>
			</nav>
		</header>
		<!-- Main content !-->
		<div id="content">
