<?php
	/**
	*	init the system using initiator.php
	*	@author Hugo Lindstrom 
	*	@contact hugolm84@gmail.com
	*
	*/
	require "lib/initiator.php";
	
	if(isset($_POST['username'], $_POST['password']) && !empty($_POST['username']) && !empty($_POST['password'])){ 
			new User($_POST['username'], $_POST['password']);
	}

	if(isset($_GET['a']) && !empty($_GET['a']) && $_GET['a'] === "logout" )
		user::loggOut();
	
	/**
	
	*	@include_template($page)
	*	very simple function, takes _GET
	*	and returns page.
	*	usage: index.php?p=account 
	*	includes account.php
	*/
	function include_template($page){
		if(user::loggedIn()){
			switch($page){
				case "account" : $include = "view/userHome.php";
				break;
				case "admin" : $include = (user::isAdmin()) ? "view/adminHome.php" : "view/userHome.php";
				break;
				case "about" : $include = "view/about.php";
				break;
				default: $include = "view/userHome.php";
				break;
			}
		}	
		else
		{
			switch($page) {
				case "about" : $include = "view/about.php";
				break;
				default : $include = "view/body.php";
				break;
			}
		}
	
		return $include;
	
	}

	require_once("view/header.php");

	$incPage = (isset($_GET['p']) && !empty($_GET['p'])) ? $_GET['p'] : "";
	require_once(include_template($incPage));
	require_once("view/footer.php");
?>
