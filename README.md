# PHP-Login
A simple login system using bcrypt and PDO

## Alter Config.class.php and Database.class.php
### Database
		The database class is a singleton and implements a PDO connection.
		It support all available sql drivers you have.
		Supports SSL connections for PgSql
	Setup:
		See config.class.php
### Config
		Set all the constants to represent your setup
### DB Schema
		Run createDb.class to create dbschema and tables for pgsql
		Login with
		* user/user
		* admin/admin