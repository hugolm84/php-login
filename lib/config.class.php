<?php

class Config{

	/**
	*	@class Config
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*/

	const 	version = "1.0";

	/**	
	*	@Database Config
	*
	*	Implements the PDO, so choose 
	*	driver by choice.
	*/
	const	db_driver = "pgsql";
	const	db_charset = "UTF-8";
	const 	db_path = ":memory";
	const 	db_host = "127.0.0.1";	// Might need localhost
	const 	db_name = "";
	const 	db_user = "";
	const 	db_password = "";
	const	table_prefix = ""; 	// If pgsql this should be db_schema + a dot eg database.
	const 	db_schema = "";		// If pgsql this should be database_name

	/**
	*	Website parameters 
	*/
	const	website_name = "My pretty website";
	const	email_adress = "mypretty@email.com";
	const	base_url = "http://localhost";

	/**	
	*	Hash Salts
	*	Change all of these to your custom salt in a similar fashion
	*/
	const	sess_key = 'Mz(UzZ7sACKkk1W:PqmATrb~<e|1f<kWr.87*!73b#srCuXt]Q;A1B:*l';
	const	salt_length = 20;

	/**
	*	Time
	**/
	public static $time;
	const timezone = "Europe/Stockholm";

	/**
	*	@administration
	*/
	
	const default_admin_group = 2;
	
	/**
	*	@Output PHP errors
	*/
	const debug = true;

	/**
	* Begin
	**/
	public function __construct(){
		/**
		*	@date-time
		*/
		
		date_default_timezone_set(config::timezone); 
		self::$time = time();
	}

	/**
	*	@debug
	*	Print php errors or not?
	*	debug should be False if site
	*	is live.
	*/
	public static function debug(){
	
		if(self::debug) {
			
			ini_set("display_errors", "1");
			error_reporting(E_ALL);
			ini_set('error_reporting', E_ALL);
		
		} else {
		
			ini_set("display_errors", "0");
			error_reporting(0);
			ini_set('error_reporting', 0);
		}
			
	}

}
