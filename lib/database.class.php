<?php


class Database {

	/**
	*	@class Database
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*
	*	Singelton class to prevent 
	* 	unnecessary database connections
	* 
	* 	Example usage:
	* 
	*	$stmt = Database::getInstance()->prepare("SELECT ...
	*	$stmt->bindParam(...
	*	$stmt->execute();
	*	$result = $stmt->fetchAll();
	*/

	private	static	$dbconn;
	private	static	$db_instance;
	private	static	$db;
	public	static	$config;
	/**
	*	PgSql ssl, eg ";sslmode=require;"
	**/
	const		ssl = "";
	
	private function __construct() { 
	
		try{
			$this->setDriver();
			self::$db = new PDO($this->dbconn, config::db_user, config::db_password);
		}
		catch(PDOException $e){
			print_r($e->getMessage());
		}
	  
	}


    public static function getInstance(){

		if (!self::$db_instance){
			self::$db_instance = new Database();

			self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
			
			if(config::debug){
				self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			}
			self::$config = new Config();
		}
		
		return self::$db;
	}


	/**
	 *	@private function setDriver()
	 *	Called by the constructor
	 *	sets the appropriate connection
	 *	for the seleceted driver.
	 *	Dies if driver dont exist or is invalid.
	*/
	private function setDriver(){

	try{	
		
		switch(config::db_driver){
			
			case "pgsql":$this->dbconn = "pgsql:dbname=".config::db_name.";host=".config::db_host.self::ssl;
			break;
			case "sqlite":$this->dbconn = "sqlite:".config::db_path; 
			break;
			case "sqlite2":$this->dbconn = "sqlite2:".config::db_path; 
			break;
			case "sqlite3":$this->dbconn = "sqlite:".config::db_path; 
			break;
			case "mysql":$this->dbconn = "mysql:host=".config::db_host.";dbname=".config::db_name;
			break;
			case "firebird":$this->dbconn = "firebird:dbname=".config::db_host.":".config::db_path;
			break;
			case "informix":$this->dbconn = "informix:DSN=".config::db_name;
			break;
			case "oracle":$this->dbconn = "OCI:dbname=".config::db_name.";charset=".self::db_charset;
			break;
			case "odbc":$this->dbconn = "odbc:Driver={Microsoft Access Driver (*.mdb)};Dbq=".config::db_path.";Uid=".self::db_user;
			break;
			case "dblib":$this->dbconn = "dblib:host=".config::db_host.":".self::db_port.";dbname=".config::db_name;
			break;
			case "ibm":$this->dbconn = "ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE=".config::db_name."; HOSTNAME=1.2.3,4;PORT=56789;PROTOCOL=TCPIP;";
			break;
			default: throw new Exception("Database Driver: Invalid driver name <strong>(".config::db_driver.")</strong>\n\r");
			break;
		}
	}
		catch(Exception $e){
		
		print (get_class().' : Could not use '.config::db_driver);	
		print (get_class().' : Available Drivers: ');
		
		foreach(PDO::getAvailableDrivers() as $driver){
			print (get_class().' : '.$driver);
		}
		die('Database Error');
		}
	}
	
	

}
