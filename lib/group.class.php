<?php

class Group extends User{
	/**
	*	@class Group
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*/
	public function getGroup(){

		$stmt = $this->db->prepare("
			SELECT group_id as id, group_name as name 
			FROM ".config::table_prefix."groups 
			WHERE group_id = :obj_group_id 
			LIMIT 1
		");

		$stmt->execute(array(':obj_group_id' => $this->user->group_id));
		return $this->user->group = $stmt->fetch(PDO::FETCH_OBJ); 

	}

}