<?php
	/**
	*	@file initiator
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*
	*	This file is the initiator. Include this and it will start 
	*	the cheeese.
	*/
	
	/**
	* @autoload the classes.
	*/
	function __autoload($class) {
		$filename = lcfirst($class) . '.class.php';
		require_once $filename;
	}

	/**
	*	@begin
	*/

	/* start the session */
	session_start();
	/* config::debug() prints php errors or not */
	config::debug();
	
?>
