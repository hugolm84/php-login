<?php

class testDB extends Config{
	/**
	* TestDB
	*	This will create DB schema for pgsql
	*/
	private static $db;

	public function __construct(){
		if(config::db_driver == "pgsql"){
			self::$db = Database::getInstance();
			$this->testDB();
		}else
			die("You need to implement your own db creations! Postgres is only supported");
	}
	private function testDB(){
		
		print "Checking db!";
		$tables = self::$db->prepare("SELECT table_schema,table_name 
			FROM information_schema.tables
			WHERE table_schema = :prefix
			AND (table_name = 'users' OR table_name = 'groups')
			ORDER BY table_schema,table_name;");
		$prefix = self::db_schema;
		$tables->bindParam(":prefix", $prefix, PDO::PARAM_STR);
		$tables->execute();
		$result = $tables->fetchAll(PDO::FETCH_OBJ);
		
		if(count($result) != 2 ){
			$this->createDB();
		}
	}
	
	private function createDB(){

		$db_schema = self::db_schema;
		$groupTable = $db_schema.".groups";
		$userTable = $db_schema.".users";

		$stmt = self::$db->prepare("CREATE SCHEMA {$db_schema}")->execute();
		
		
		$stmt = self::$db->prepare("CREATE TABLE {$userTable} (
			user_id integer NOT NULL,
			username varchar(225) NOT NULL,
			password varchar(225) NOT NULL,
			email varchar(150) NOT NULL,
			group_id integer DEFAULT '1',
			salt varchar(120) DEFAULT NULL,
			PRIMARY KEY (user_id)
			);
		");
		$stmt->execute();
		
		$stmt = self::$db->prepare("CREATE TABLE {$groupTable} (
			group_id integer NOT NULL,
			group_name varchar(225) NOT NULL,
			PRIMARY KEY (group_id)
			);
		");
		$stmt->execute();
		
		$stmt = self::$db->prepare("INSERT INTO {$groupTable} (group_id, group_name) VALUES
			(1, 'User'),
			(2, 'Admin');
		");
		$stmt->execute();
		
		// Generates new hashes
		$admin = hash::generatePwHash("admin");
		$user = hash::generatePwHash("user");
		
		$stmt = self::$db->prepare("INSERT INTO {$userTable} (user_id, username, password, email, group_id, salt) VALUES
			(1, 'admin', '".$admin->encrypted."', 'bla@bla.se', 2, '".$admin->salt."'),
			(2, 'user', '".$user->encrypted."', 's@ss.se', 1, '".$user->salt."');
		");
		$stmt->execute();
		
	}

}