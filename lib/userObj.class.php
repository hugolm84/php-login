<?php

class UserObj {
	/**
	*	@class UserObj
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*	Fetches a userObject from database
	*	Strips strings and sets entities 
	**/

	public function __get($prop) {
		return $this->{$prop};
	}
	
	public function __set($prop, $val) {
		$this->{$prop} = is_string($val) ? strip_tags(htmlentities($val)) : $val;
	}
	
	public function __toString(){
		return isset($this->username) ? $this->username : "W00t";
	}
}