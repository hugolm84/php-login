<?php

class Admin extends User{
	/**
	*	@class Admin
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*/
	public function checkAdmin(){
	
		if($this->user->group_id == config::default_admin_group){
			return $this->user->isAdmin = true;
		}
		return $this->user->isAdmin = false;
	}
}
