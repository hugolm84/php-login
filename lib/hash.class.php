<?php

class Hash extends Config{

	/**
	*	@class Hash
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*
	*	This class takes care of all the hashin
	*/

	/**
	*	Generates password and salt hash
	* 	Store these in db
	**/
	public static function generatePwHash($input, string $salt = NULL){

		$secure = new SecureHash();
		// the encrypted version of the password (for database storage)
		$encrypted = $secure->create_hash($input, $salt);
		return (object)array("encrypted" => $encrypted, "salt" => $salt);
	}

	/**
	* validates the input password
	*/
	public static function validatePassword($user, $pwd){
		$stmt =  Database::getInstance()->prepare("SELECT user_id, password, salt FROM ".config::table_prefix."users
									WHERE username = :obj_username
									LIMIT 1");
		$stmt->bindParam(':obj_username', $user, PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);

		if(isset($result->user_id)){
			$secure = new SecureHash();
			return $secure->validate_hash($pwd, $result->password, $result->salt);
		}
		return false;
	}

}

