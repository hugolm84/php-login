<?php

class User {
	/**
	*	@class User
	*	@author Hugo Lindstrom
	*	@contact hugolm84@gmail.com
	*/
	private $obj_password;
	private $obj_username;

	protected $user;
	protected $db;

	public function __construct($obj_user = NULL, $obj_password = NULL){

		if(!self::loggedIn()){
			try{
				/**
				*	Strip the username
				**/
				$this->obj_username = strip_tags(htmlentities($obj_user));

				if(!hash::validatePassword($this->obj_username, $obj_password))
					throw new Exception("Username or Password incorrect!");

				session_regenerate_id(true);

				$this->db = Database::getInstance();
				// Get user
				$this->getUserFromDb();

			}catch(Exception $e){
				print $e->getMessage();
			}
		}
	}

	private function getUserFromDb() {

		$stmt = $this->db->prepare("
			SELECT user_id, username, email, group_id
			FROM ".config::table_prefix."users 
			WHERE username = :obj_username
			LIMIT 1
		");

		$stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'UserObj');
		$stmt->execute(array(':obj_username' => $this->obj_username));

		$this->user = $stmt->fetch();

		if(isset($this->user->user_id)){
			// get admin creds, and groups
			admin::checkAdmin();
			group::getGroup();
			$_SESSION["user"] = $this->user;
		}
		else {
			unset($_SESSION['user']);
			throw new Exception("Username or Password incorrect!");
		}

	}

	/**
	*	Static user functions
	**/
	public static function getUser(){
		if(self::loggedIn())
			return (object)$_SESSION["user"];
		return NULL;
	}
	
	public static function isAdmin(){

		return self::getUser()->isAdmin;
	}

	public static function loggedIn(){

		return isset($_SESSION["user"]);
	}

	public static function loggOut(){

		unset($_SESSION['user']);
		session_unset();
		session_destroy();
		header("Location: index.php");
	}
}